/**
 * @name Settings
 * @desc This file contains the default persistent storage and the namespacing
 *      for the store variable.
 * @since Mar 2 2017
 */

// Imports
import store from 'store2';

// Default Settings
let defaultSettings = {
    // Potected Settings
    _user : {
        username: null,
        email: null,
        token: null,
        userId: null,
    },
    _recordHistory: [],
    _offlineMode: false,

    // Public Settings
    displayAds: true,
    theme: {
        selected: 'Dark',
        options: ['Dark'],
    },
    queueMaxSize: {
        selected: 5,
        options: [5, 10, 15],
    }
}

// Apply the settings
const settings = store.namespace('settings');

// If the settings don't exist set them
if (store.settings.size() === 0) {
    settings(defaultSettings);
}

/**
 * @desc This method will reset the settings to their default values
 * @since May 31 2017
 */
function resetDefaultSettings() {
    // Clear the existing values
    settings.clearAll();

    // Assign the default values
    settings(defaultSettings);
};

// Exports
export { settings, resetDefaultSettings };
