/**
 * @name Loading Messages
 * @desc This file contains the configuration for the random loading messages
 * @since May 4 2017
 */

let loadingMessages = [
    {
        text: 'Loading',
    },
    {
        text: 'Letting Tarkus handle it',
    },
    {
        text: 'Reticulating splines',
    },
    {
        text: 'Preparing for titanfall',
    },
    {
        text: 'Complaining in group chat',
    },
    {
        text: 'Downloading Uplay',
    },
    {
        text: 'Hacking all IPs simultaneously',
    },
    {
        text: '🎵 Browsing music folder 🎵',
    },
    {
        text: 'Deleteing "that" folder',
    },
    {
        text: 'Stuff and things',
    },
    {
        text: 'Wait nevermind, I shouldn\'t have done that',
    },
    {
        text: 'Loading some more',
    },
    {
        text: 'Pinging Antartica servers',
    },
    {
        text: 'Instalocking Hanzo',
    },
    {
        text: 'Checking Discord messages',
    },
    {
        text: 'Gettin\' Hype!',
    },
    {
        text: 'Raising Confidant Ranks',
    },
    {
        text: 'Preparing for All-Out-Attack!'
    },
    {
        text: 'Encouraging skeletons',
    },
    {
        text: 'Fixing thermal drill',
        source: '@Veloxio',
    },
    {
        text: 'Deploying medic bag',
        source: '@Veloxio',
    },
    {
        text: 'Preparing mask',
        source: '@Veloxio',
    },
    {
        text: 'Assaulting cash on legs',
    },
    {
        text: 'Now with less bees!',
        source: '@KnowsTooMuch'
    },
    {
        text: 'Preparing to optimize... Something?',
        source: '@KnowsTooMuch'
    },
    {
        text: 'Have any questions? Yell into the void for answers',
        source: '@KnowsTooMuch'
    }
];

// Exports
export default loadingMessages;
