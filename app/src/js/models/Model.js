/**
 * @name Model
 * @desc This class is the data model class. It will throw errors if
 *      you don't override the required methods.
 * @since Feb 24 2017
 */
class Model {
    /**
     * @desc Constructor
     */
    constructor() {
    }

    /**
     * @desc This method will override the toString method to
     *      return a json representation of its self.
     * @since Feb 24 2017
     */
    toString() {
        return JSON.stringify(this);
    }

    /**
    * @desc This method will save an instance of the object created to the local
    *       database and the remote database.
    * @since Feb 26 2017
    * @type Abstract
     */
    save() {
        // Throw an error if not overriding
        throw 'Method save not overriden';
    }

    /**
     * @desc This method will wrap the tables where method and return
     *      WhereClause or a Collection based on the the parameter provided.
     * @since Feb 28 2017
     * @type Abstract
     */
    static where() {
        // Throw an error if not overriding
        throw 'Static method where not overriden';
    }

    /**
     * @desc This method will sync the instances of this class from the remote
     *      database to the local database.
     * @since Feb 24 2017
     * @type Abstract
     */
    static sync() {
        // Throw an error if not overriding
        throw 'Static method sync not overriden';
    }
}

// Exports
export default Model;
