/**
 * @name Record
 * @desc This class is the data model for a record
 * @since Feb 24 2017
 */

// Imports
import request from 'superagent/superagent';
import db from  '../db.js';
import config from '../config/config.js';
import { settings } from '../settings.js';
import Model from './Model.js';
import Session from './Session.js';

class Record extends Model {
    /**
     * @desc Constructor
     */
    constructor(args) {
        super();

        // Init properties
        this.recordId = args.recordId ? args.recordId : new Date().getTime().toString(16);
        this.gameId = args.gameId ? args.gameId : null;
        this.completionStatus = args.completionStatus ? args.completionStatus : 'queue';
        this.offlineOnly = args.offlineOnly ? args.offlineOnly : 0;
        this.pendingDelete = args.pendingDelete ? args.pendingDelete : 0;
        this.version = args.version ? args.version : 0;
    }

    /**
     * @desc This method overrides the inheritted save method.
     * @since Feb 26 2017
     * @param Object args
     * @returns Promise
     */
    save(args) {
        // Create Promise
        let savePromise = new Promise((resolve, reject) => {
            // Store an array of the promises
            let promises = [];

            // Increase version
            this.version++;

            // Set offline only
            this.offlineOnly = 1;

            // save locally
            let localPromise = db.records.put(this);

            // Append to promises
            promises.push(localPromise);

            // If online
            if (!settings()._offlineMode) {
                // Get user token
                let token = settings('_user').token;

                // Save a new instance
                if (args && args.serverMethod == 'POST') {
                    // Request a new session be created
                    let apiReq = request('POST', config.wrapditUrl + 'record')
                        .set('access-token', token)
                        .type('form')
                        .send({
                            game_id: this.gameId,
                            status: this.status
                        });

                    // Append the promises
                    promises.push(apiReq);

                    // On request response set the recieved session id
                    apiReq.then((response) => {
                        // Get the new record id
                        let newRecordId = JSON.parse(response.text).record.record_id;

                        // Query related sessions
                        let sessionQuery = Session.where('recordId').equals(this.recordId).toArray();

                        // Push the query to the promise array
                        promises.push(sessionQuery);

                        // Cascade the update of the sessions record id
                        sessionQuery.then((results) => {
                            // Loop through the records
                            results.forEach((session) => {
                                // Save changes
                                session.recordId = newRecordId;
                                let sessionCascade = session.save();

                                // Push the promise to the promise array
                                promises.push(sessionCascade);
                            })
                        });

                        // Set the record id
                        this.recordId = newRecordId;
                        this.offlineOnly = 0;

                        // Save locally again
                        let updatePromise = db.records.put(this);

                        // Push the save to the promise array
                        promises.push(updatePromise);
                    })

                    // If save to server failed mark it as offline only
                    .catch((ex) => {
                        // Set offline only
                        this.offlineOnly = 1;
                        let updatePromise = db.records.put(this);

                        // Push the save to the promise array
                        promises.push(updatePromise);
                    });
                }

                else if (args && args.serverMethod == 'PATCH') {
                    // Request a new session be created
                    let apiReq = request('PATCH', config.wrapditUrl + 'record')
                        .set('access-token', token)
                        .type('form')
                        .send({
                            record_id: this.recordId,
                            status: this.status,
                        });

                    // Append the promises
                    promises.push(apiReq);
                }
            }

            // On all promise resolved
            Promise.all(promises).then(() => {
                resolve(this);
            })

            // Catch exceptions
            .catch((ex) => {
                reject(ex);
            });
        });

        // Return save promise
        return savePromise;
    }

    /**
     * @desc This method overrides the inheritted where method.
     * @since Feb 28 2017
     * @param
     *      Object keyPath
     *      String keyPath
     * @returns
     *      WhereClause
     *      Collection
     */
    static where(keyPath) {
        // If keypath is null return all
        if (!keyPath) {
            return db.records.toCollection();
        }

        // Return a queryable WhereClause
        return db.records.where(keyPath);
    }

    /**
     * @desc This method overrides the inheritted sync method.
     * @since Feb 24 2017
     * @param Object query
     * @returns Promise
     */
    static sync(query) {
        // New promise
        let syncPromise = new Promise((resolve, reject) => {
            // Sync put
            this.syncPut().then(() => {
                // Sync get
                this.syncGet(query).then((records) => {
                    resolve(records);
                })

                // Rethrow any exceptions
                .catch((ex) => {
                    throw(ex);
                });
            })

            // Rethrow any exceptions
            .catch((ex) => {
                reject(ex);
            });
        });

        // Return the promise
        return syncPromise;
    }

    /**
     * @desc This method handles 1 part of the bidirectionla sync. The fetching
     *      from the server and the updating of the local copy.
     * @since Feb 26 2017
     * @param Object query
     * @returns Promise
     */
    static syncGet(query) {
        // New Promise
        let getPromise = new Promise((resolve, reject) => {
            // Get user token
            let token = settings('_user').token;

            // Request all users records from API
            let apiReq = request('GET', config.wrapditUrl + 'record')
                .set('access-token', token);

            // If there are query params
            if (query) {
                apiReq.query(query);
            }

            // Store the records
            let records = [];

            // Once response is recieved
            apiReq.then((response) => {
                // Parse the api records
                let remoteRecords = JSON.parse(response.text).records;

                // Loop through each record
                remoteRecords.forEach((remoteRecord) => {
                    // Try to get the local copy of the record
                    this.where('recordId').equals(remoteRecord.record_id).first((localRecord) => {
                        // If localRecord is undefined
                        if (!localRecord) {
                            // Create a new record
                            localRecord = new Record({
                                recordId: remoteRecord.record_id,
                                gameId: remoteRecord.game_id,
                                completionStatus: remoteRecord.completion_status,
                                version: remoteRecord.version,
                                offlineOnly: 0,
                            });

                            // Save the changes
                            db.records.put(localRecord);
                        }

                        // If an entry was found and we have the newer version
                        else if (remoteRecord.version > localRecord.version) {
                            // Add all the info from the remote record
                            localRecord.recordId = remoteRecord.record_id;
                            localRecord.gameId = remoteRecord.game_id;
                            localRecord.completionStatus = remoteRecord.completion_status;
                            localRecord.version = remoteRecord.version;
                            localRecord.offlineOnly = 0;

                            // Save the changes
                            db.records.put(localRecord);
                        }

                        // Store the record
                        records.push(localRecord);
                    });
                });

                // Resolve the promise
                resolve(records);
            })

            // Re throwing any rejections
            .catch((ex) => {
                reject(ex);
            });
        });

        // Return the promise
        return getPromise;
    }

    /**
     * @desc This method handles 1 part of the bidirectionla sync. Pushing of
     *      local records to the server.
     * @since Feb 26 2017
     * @returns Promise
     */
    static syncPut() {
        // New promise
        let putPromise = new Promise((resolve, reject) => {
            // Get user token
            let token = settings('_user').token;

            // Get all the records from the local database
            db.records.toArray((results) => {
                // If there are no results to send resolve
                if (results.length == 0) {
                    resolve();
                }

                // Else send all local records to the server
                else {
                    // Create request to server
                    let apiReq = request('PUT', config.wrapditUrl + 'record')
                        .set('access-token', token)
                        .type('form')
                        .send({ client_data: JSON.stringify(results) });

                    // On server response
                    apiReq.then((response) => {
                        resolve();
                    })

                    // Re throwing any rejections
                    .catch((ex) => {
                        reject(ex);
                    });
                }
            });
        });

        // Return promise
        return putPromise;
    }

    /**
     * @desc This method will delete the offline only versions once
     *      they exist on the server.
     * @since Mar 17 2016
     * @returns Promise
     */
    static syncDelete() {
        // Purge all session that are offline only
        let deleteQuery = this.where('offlineOnly').equals(1).delete();

        // Return the query promise
        return deleteQuery;
    }
}

// Map Class
db.records.mapToClass(Record);

// Exports
export default Record;
