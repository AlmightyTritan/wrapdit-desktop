/**
 * @name Session
 * @desc This class is the data model for a session
 * @since Feb 24 2017
 */

// Imports
import request from 'superagent/superagent';
import db from  '../db.js';
import config from '../config/config.js';
import { settings } from '../settings.js';
import Model from './Model.js';

class Session extends Model {
    /**
     * @desc Constructor
     */
    constructor(args) {
        super();

        // Init properties
        this.sessionId = args.sessionId ? args.sessionId : new Date().getTime().toString(16);
        this.recordId = args.recordId ? args.recordId : null;
        this.duration = args.duration ? args.duration : 0;
        this.date = args.date ? args.date : new Date().toISOString();
        this.offlineOnly = args.offlineOnly ? args.offlineOnly : 0;
        this.pendingDelete = args.pendingDelete ? args.pendingDelete : 0;
        this.version = args.version ? args.version : 0;
    }

    /**
     * @desc This method overrides the inheritted save method.
     * @since Feb 26 2017
     * @param Object args
     * @returns Promise
     */
    save(args) {
        // Create a new promise
        let savePromise = new Promise((resolve, reject) => {
            // Store an array of the promises
            let promises = [];

            // Increase version
            this.version++;

            // Set offline only
            this.offlineOnly = 1;

            // save locally
            let localPromise = db.sessions.put(this);

            // Append to promises
            promises.push(localPromise);

            // If online
            if (!settings()._offlineMode) {
                // Get user token
                let token = settings('_user').token;

                // Save a new instance
                if (args && args.serverMethod == 'POST') {
                    // Request a new session be created
                    let apiReq = request('POST', config.wrapditUrl + 'session')
                        .set('access-token', token)
                        .type('form')
                        .send({
                            record_id: this.recordId,
                            duration: this.duration,
                        });

                    // Append the promises
                    promises.push(apiReq);

                    // On request response set the recieved session id
                    apiReq.then((response) => {
                        this.sessionId = JSON.parse(response.text).session.session_id;
                        this.offlineOnly = 0;

                        // save locally again
                        let updatePromise = db.sessions.put(this);

                        // Push the save to the promise array
                        promises.push(updatePromise);
                    })

                    // If save to server failed mark it as offline only
                    .catch((ex) => {
                        // Set offline only
                        this.offlineOnly = 1;
                        let updatePromise = db.sessions.put(this);

                        // Push the save to the promise array
                        promises.push(updatePromise);
                    });
                }

                else if (args && args.serverMethod == 'PATCH') {
                    // Request a new session be created
                    let apiReq = request('PATCH', config.wrapditUrl + 'session')
                        .set('access-token', token)
                        .type('form')
                        .send({
                            session_id: this.sessionId,
                            date: this.date,
                            duration: this.duration,
                        });

                    // Append the promises
                    promises.push(apiReq);
                }
            }

            // On promises resolve
            Promise.all(promises).then(() => {
                resolve(this);
            })

            // Catch exceptions
            .catch((ex) => {
                reject(ex);
            });
        });

        // Return save promise
        return savePromise;
    }

    /**
     * @desc This method overrides the inheritted where method.
     * @since Feb 28 2017
     * @param
     *      Object keyPath
     *      String keyPath
     * @returns
     *      WhereClause
     *      Collection
     */
    static where(keyPath) {
        // If keypath is null return all
        if (!keyPath) {
            return db.sessions.toCollection();
        }

        // Return a queryable WhereClause
        return db.sessions.where(keyPath);
    }

    /**
     * @desc This method overrides the inheritted sync method.
     * @since Feb 24 2017
     * @param Object query
     * @returns Promise
     */
    static sync(query) {
        // New promise
        let syncPromise = new Promise((resolve, reject) => {
            // Sync put
            this.syncPut().then(() => {
                // Sync get
                this.syncGet(query).then((records) => {
                    resolve(records);
                })

                // Rethrow any exceptions
                .catch((ex) => {
                    throw(ex);
                });
            })

            // Rethrow any exceptions
            .catch((ex) => {
                reject(ex);
            });
        });

        // Return the promise
        return syncPromise;
    }

    /**
     * @desc This method handles 1 part of the bidirectionla sync. The fetching
     *      from the server and the updating of the local copy.
     * @since Feb 26 2017
     * @returns Promise
     */
    static syncGet(query) {
        // New Promise
        let getPromise = new Promise((resolve, reject) => {
            // Get user token
            let token = settings('_user').token;

            // Request all users records from API
            let apiReq = request('GET', config.wrapditUrl + 'session')
                .set('access-token', token);

            // If there are query params
            if (query) {
                apiReq.query(query);
            }

            // Store the records
            let sessions = [];

            // Once response is recieved
            apiReq.then((response) => {
                // Parse the api records
                let remoteSessions = JSON.parse(response.text).sessions;

                // Loop through each record
                remoteSessions.forEach((remoteSession) => {
                    // Try to get the local copy of the record
                    this.where('sessionId').equals(remoteSession.session_id).first((localSession) => {
                        // If localRecord is undefined
                        if (!localSession) {
                            // Create a new session
                            localSession = new Session({
                                sessionId: remoteSession.session_id,
                                recordId: remoteSession.record_id,
                                duration: remoteSession.duration,
                                date: remoteSession.date,
                                version: remoteSession.version,
                                offlineOnly: 0,
                            });

                            // Save the changes
                            db.sessions.put(localSession);
                        }

                        // If an entry was found and we have the newer version
                        else if (remoteSession.version > localSession.version) {
                            // Add all the info from the remote session
                            localSession.sessionId = remoteSession.session_id;
                            localSession.recordId = remoteSession.record_id;
                            localSession.duration = remoteSession.duration;
                            localSession.date = remoteSession.date;
                            localSession.version = remoteSession.version;
                            localSession.offlineOnly = 0;

                            // Save the changes
                            db.sessions.put(localSession);
                        }

                        // Store the session
                        sessions.push(localSession);
                    });
                });

                // Resolve the promise
                resolve(sessions);
            })

            // Re throwing any rejections
            .catch((ex) => {
                reject(ex);
            });
        });

        // Return the promise
        return getPromise;
    }

    /**
     * @desc This method handles 1 part of the bidirectionla sync. Pushing of
     *      local records to the server.
     * @since Feb 26 2017
     * @returns Promise
     */
    static syncPut() {
        // New promise
        let putPromise = new Promise((resolve, reject) => {
            // Get user token
            let token = settings('_user').token;

            // Get all the records from the local database
            db.sessions.toArray((results) => {
                // If there are no results to send resolve
                if (results.length == 0) {
                    resolve();
                }

                // Else send all local records to the server
                else {
                    // Create request to server
                    let apiReq = request('PUT', config.wrapditUrl + 'session')
                        .set('access-token', token)
                        .type('form')
                        .send({ client_data: JSON.stringify(results) });

                    // On server response
                    apiReq.then((response) => {
                        resolve();
                    })

                    // Re throwing any rejections
                    .catch((ex) => {
                        reject(ex);
                    });
                }
            });

        });

        // Return promise
        return putPromise;
    }

    /**
     * @desc This method will delete the offline only versions once
     *      they exist on the server.
     * @since Mar 17 2016
     * @returns Promise
     */
    static syncDelete() {
        // Purge all session that are offline only
        let deleteQuery = this.where('offlineOnly').equals(1).delete();

        // Return the query promise
        return deleteQuery;
    }
}

// Map Class
db.sessions.mapToClass(Session);

// Exports
export default Session;
