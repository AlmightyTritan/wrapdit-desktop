/**
 * @name Game
 * @desc This class is the data model for a game
 * @since Feb 24 2017
 */

// Imports
import request from 'superagent/superagent';
import moment from 'moment';
import db from  '../db.js';
import config from '../config/config.js'
import { settings } from '../settings.js';
import Model from './Model.js';
import Record from './Record.js';

class Game extends Model {
    /**
     * @desc Constructor
     */
    constructor(args) {
        super();

        // Init properties
        this.gameId = args.gameId ? args.gameId : new Date().getTime().toString(16);
        this.igdbId = args.igdbId ? args.igdbId : null;
        this.title = args.title ? args.title : null;
        this.ttbAverageIGDB = args.ttbAverageIGDB ? args.ttbAverageIGDB : null;
        this.ttbAverageNormal = args.ttbAverageNormal ? args.ttbAverageNormal : null;
        this.ttbMedianNormal = args.ttbMedianNormal ? args.ttbMedianNormal : null;
        this.ttbFastestNormal = args.ttbFastestNormal ? args.ttbFastestNormal : null;
        this.ttbSlowestNormal = args.ttbSlowestNormal ? args.ttbSlowestNormal : null;
        this.ttbEntriesNormal = args.ttbEntriesNormal ? args.ttbEntriesNormal : null;
        this.gsCompleted = args.gsCompleted ? args.gsCompleted : null;
        this.gsAbandoned = args.gsAbandoned ? args.gsAbandoned : null;
        this.gsActive = args.gsActive ? args.gsActive : null;
        this.gsBacklog = args.gsBacklog ? args.gsBacklog : null;
        this.coverImage = args.coverImage ? args.coverImage: null;
        this.screenshots = args.screenshots ? args.screenshots : [];
        this.summary = args.summary ? args.summary : null;
        this.rating = args.rating ? args.rating : null;
        this.releaseDate = args.releaseDate ? args.releaseDate : null;
        this.developer = args.developer ? args.developer : null;
        this.offlineOnly = args.offlineOnly ? args.offlineOnly : 0;
        this.pendingDelete = args.pendingDelete ? args.pendingDelete : 0;
        this.version = args.version ? args.version : null;
    }

    /**
     * @desc This method overrides the inheritted save method.
     * @since Feb 26 2017
     * @param Object args
     * @returns Promise
     */
    save(args) {
        // Create new promise
        let savePromise = new Promise((resolve, reject) => {
            // Store an array of the promises
            let promises = [];

            // Increase version
            this.version++;

            // Set offline only
            this.offlineOnly = 1;

            // save locally
            let localPromise = db.games.put(this);

            // Append to promises
            promises.push(localPromise);

            // If online
            if (!settings()._offlineMode) {
                // Get user token
                let token = settings('_user').token;

                // If saving a new instance
                if (args && args.serverMethod == 'POST') {
                    // Send a request to create a new game
                    let apiReq = request('POST', config.wrapditUrl + 'game')
                        .set('access-token', token)
                        .type('form')
                        .send({
                            igdb_id: this.igdbId,
                            title: this.title
                        });

                    // Append the promises
                    promises.push(apiReq);

                    // On request complete
                    apiReq.then((response) => {
                        // Get the remote game
                        let remoteGame = JSON.parse(response.text).game;

                        // Set the retrieved data
                        this.gameId = remoteGame.game_id;
                        this.ttbAverageNormal = remoteGame.time_to_beat.normal.average;
                        this.ttbMedianNormal = remoteGame.time_to_beat.normal.median;
                        this.ttbFastestNormal = remoteGame.time_to_beat.normal.fastest;
                        this.ttbSlowestNormal = remoteGame.time_to_beat.normal.slowest;
                        this.ttbEntriesNormal = remoteGame.time_to_beat.normal.entries;
                        this.gsCompleted = remoteGame.global_status.completed;
                        this.gsAbandoned = remoteGame.global_status.abandoned;
                        this.gsActive = remoteGame.global_status.active;
                        this.gsBacklog = remoteGame.global_status.backlog;
                        this.offlineOnly = 0;

                        // Save locally again
                        let updatePromise = db.games.put(this);

                        // Push the save to the promise array
                        promises.push(updatePromise);
                    })

                    // If save to server failed mark it as offline only
                    .catch((ex) => {
                        // Set offline only
                        this.offlineOnly = 1;
                        let updatePromise = db.games.put(this);

                        // Push the save to the promise array
                        promises.push(updatePromise);
                    });
                }
            }

            // On all promise resolve
            Promise.all(promises).then(() => {
                // Resolve
                resolve(this);
            })

            // Catch exceptions
            .catch((ex) => {
                reject(ex);
            })
        });

        // Return save promise
        return savePromise;
    }

    /**
     * @desc This method will fetch any properties needed from the IGDB api.
     * @since Mar 3 2017
     * @param Int igdbId
     * @returns Promise
     */
    syncIGDB(igdbId) {
        //New Promise
        let syncPromise = new Promise((resolve, reject) => {
            // If any new id was passed change this instances igdb id
            if (igdbId) {
                this.igdbId = igdbId;
            }

            // If this instance has no igdb then reject
            if (!this.igdbId) {
                reject('This instance of "Game" has no IGDB ID.');
            }

            // Make a request to the IGDB API
            let igdbReq = request('GET', config.igdbUrl + 'games/' + this.igdbId + '?fields=*')
                .set('X-Mashape-Key', config.igdbKey)
                .set('Accept', 'application/json');

            // On request response
            igdbReq.then((response) => {
                // Parse the response
                let igdbData = JSON.parse(response.text)[0];

                // Assign the values to the game
                this.summary = igdbData.summary ? igdbData.summary : null;
                this.rating = igdbData.aggregated_rating ? igdbData.aggregated_rating : null;
                this.releaseDate = igdbData.first_release_date ? moment(igdbData.first_release_date).format() : null;
                this.coverImage = igdbData.cover ? igdbData.cover.cloudinary_id : null;
                this.ttbAverageIGDB = igdbData.time_to_beat ? igdbData.time_to_beat.normally : null;

                // Get the image ids for the screenshots
                if (igdbData.screenshots) {
                    // Loop through the recieved screenshot
                    for (let remoteScreenshot of igdbData.screenshots) {
                        // Loop through existing screenshot ids
                        for (let screenshot of this.screenshots) {
                            // If this id already exists skip it
                            if (remoteScreenshot.cloudinary_id == screenshot) {
                                continue;
                            }

                            // Else push it to the screenshot array
                            this.screenshots.push(remoteScreenshot.cloudinary_id);
                        }
                    }
                }

                // Save the data
                db.games.put(this);

                // Make a request to IGDB for developer
                if (igdbData.developers) {
                    let devReq = request('GET', config.igdbUrl + 'companies/' + igdbData.developers[0] + '?fields=name')
                        .set('X-Mashape-Key', config.igdbKey)
                        .set('Accept', 'application/json');

                    // On request response
                    devReq.then((response) => {
                        // Getting the developers name
                        this.developer = JSON.parse(response.text)[0].name;

                        // Save the data
                        db.games.put(this);
                    })

                    // Rethrow any exceptions
                    .catch((ex) => {
                        throw(ex);
                    });
                }
            })

            // Rethrow any exceptions
            .catch((ex) => {
                reject(ex);
            });
        });

        // Return promise
        return syncPromise;
    }

    /**
     * @desc This method overrides the inheritted where method.
     * @since Feb 28 2017
     * @param
     *      Object keyPath
     *      String keyPath
     * @returns
     *      WhereClause
     *      Collection
     */
    static where(keyPath) {
        // If keypath is null return all
        if (!keyPath) {
            return db.games.toCollection();
        }

        // Return a queryable WhereClause
        return db.games.where(keyPath);
    }

    /**
     * @desc This method overrides the inheritted sync method.
     * @since Feb 24 2017
     * @param Object query
     * @returns Promise
     */
    static sync(query) {
        // New promise
        let syncPromise = new Promise((resolve, reject) => {
            // Sync put
            this.syncPut().then(() => {
                // Sync get
                this.syncGet(query).then((records) => {
                    resolve(records);
                })

                // Rethrow any exceptions
                .catch((ex) => {
                    throw(ex);
                });
            })

            // Rethrow any exceptions
            .catch((ex) => {
                reject(ex);
            });
        });

        // Return the promise
        return syncPromise;
    }

     /**
      * @desc This method handles fetching games from the server
      *     and the updating of the local copy.
      * @since Feb 26 2017
      * @param Object query
      * @returns Promise
      */
    static syncGet(query) {
        // New Promise
        let getPromise = new Promise((resolve, reject) => {
            // Get user token
            let token = settings('_user').token;

            // Request all users games from API
            let apiReq = request('GET', config.wrapditUrl + 'game')
                .set('access-token', token);

            // If there are query params
            if (query) {
                apiReq.query(query);
            }

            // Store the games
            let games = [];

            // Once request recieves response
            apiReq.then((response) => {
                // Parse the remote games
                let remoteGames = JSON.parse(response.text).games;

                // Loop through each game
                remoteGames.forEach((remoteGame) => {
                    // Check if we have a local copy of this game
                    this.where('gameId').equals(remoteGame.game_id).first((localGame) => {
                        // If there is no copy of this game
                        if (!localGame) {
                            // Create a new game
                            localGame = new Game({
                                gameId: remoteGame.game_id,
                                igdbId: remoteGame.igdb_id,
                                title: remoteGame.title,
                                ttbAverageNormal: remoteGame.time_to_beat.normal.average,
                                ttbMedianNormal: remoteGame.time_to_beat.normal.median,
                                ttbFastestNormal: remoteGame.time_to_beat.normal.fastest,
                                ttbSlowestNormal: remoteGame.time_to_beat.normal.slowest,
                                ttbEntriesNormal: remoteGame.time_to_beat.normal.entries,
                                gsCompleted: remoteGame.global_status.completed,
                                gsAbandoned: remoteGame.global_status.abandoned,
                                gsActive: remoteGame.global_status.active,
                                gsBacklog: remoteGame.global_status.backlog,
                                version: remoteGame.version,
                                offlineOnly: 0
                            });

                            // Save changes
                            db.games.put(localGame);
                        }

                        // Else update the games data
                        else {
                            // Update the properties
                            localGame.gameId = remoteGame.game_id;
                            localGame.igdbId = remoteGame.igdb_id;
                            localGame.title = remoteGame.title;
                            localGame.ttbAverageNormal = remoteGame.time_to_beat.normal.average;
                            localGame.ttbMedianNormal = remoteGame.time_to_beat.normal.median;
                            localGame.ttbFastestNormal = remoteGame.time_to_beat.normal.fastest;
                            localGame.ttbSlowestNormal = remoteGame.time_to_beat.normal.slowest;
                            localGame.ttbEntriesNormal = remoteGame.time_to_beat.normal.entries;
                            localGame.gsCompleted = remoteGame.global_status.completed;
                            localGame.gsAbandoned = remoteGame.global_status.abandoned;
                            localGame.gsActive = remoteGame.global_status.active;
                            localGame.gsBacklog = remoteGame.global_status.backlog;
                            localGame.version = remoteGame.version;
                            localGame.offlineOnly = 0;

                            // Save changes
                            db.games.put(localGame);
                        }

                        // Store the game
                        games.push(localGame);
                    })
                });

                // Resolve the promise
                resolve(games);
            })

            // Rethrow any exceptions
            .catch((ex) => {
                reject(ex);
            });
        });

        // Return the promise
        return getPromise;
    }

    /**
     * @desc This method handles 1 part of the bidirectionla sync. Pushing of
     *      local games to the server.
     * @since Mar 24 2017
     * @returns Promise
     */
    static syncPut() {
        // New promise
        let putPromise = new Promise((resolve, reject) => {
            // Get user token
            let token = settings('_user').token;

            // Get all the games from the local database
            db.games.toArray((results) => {
                // If there are no results to send resolve
                if (results.length == 0) {
                    resolve();
                }

                // Else send all local games to the server
                else {
                    // Create request to server
                    let apiReq = request('PUT', config.wrapditUrl + 'game')
                        .set('access-token', token)
                        .type('form')
                        .send({ client_data: JSON.stringify(results) });

                    // On server response
                    apiReq.then((response) => {
                        // Parse the games updated
                        let putGames = JSON.parse(response.text).games;

                        // Store any required promises
                        let putPromises = [];

                        // Loop through the games
                        putGames.forEach((putGame) => {
                            // If an offline game was found
                            if (putGame.offline_id) {
                                // Query games for the matching offline id
                                let offlineQuery = this.where('gameId').equals(putGame.offline_id).first();

                                // Add the query to the promises
                                putPromises.push(offlineQuery);

                                // Once query completes
                                offlineQuery.then((result) => {
                                    // Update the game found
                                    result.gameId = putGame.game_id;
                                    result.ttbAverageNormal = putGame.time_to_beat.normal.average;
                                    result.ttbMedianNormal = putGame.time_to_beat.normal.median;
                                    result.ttbFastestNormal = putGame.time_to_beat.normal.fastest;
                                    result.ttbSlowestNormal = putGame.time_to_beat.normal.slowest;
                                    result.ttbEntriesNormal = putGame.time_to_beat.normal.entries;
                                    result.gsCompleted = putGame.global_status.completed;
                                    result.gsAbandoned = putGame.global_status.abandoned;
                                    result.gsActive = putGame.global_status.active;
                                    result.gsBacklog = putGame.global_status.backlog;
                                    result.offlineOnly = 0;

                                    // Save the changes
                                    let savePromise = db.games.put(result);

                                    // Add the save changes to the promises
                                    putPromises.push(savePromise);
                                });
                            }
                        });

                        // Once all of any possible promises are resolved
                        Promise.all(putPromises).then(() => {
                            resolve();
                        })

                        // Rethrow any exceptions
                        .catch((ex) => {
                            throw ex;
                        });
                    })

                    // Re throwing any rejections
                    .catch((ex) => {
                        reject(ex);
                    });
                }
            });
        });

        // Return promise
        return putPromise;
    }
}

// Map Class
db.games.mapToClass(Game);

// Exports
export default Game;
