/**
 * @name Config
 * @desc This file contains the configurations for the application.
 *      Please replace the values before you compile and run the app.
 *      Also you gotta rename this file to "config.js" because I can't figure
 *      out a better way to handle config imports.
 * @since May 29 2017
 */

// Create the config object
let config = {
    // Debug
    debug: true,

    // API settings
    wrapditUrl: 'https://REPLACE-ME-YO/', // Gotta have a terminating slash
    igdbUrl: 'https://REPLACE-ME-YO/', // Gotta have a terminating slash
    igdbKey: 'REPLACE-ME-YO-WITH-A-PROPER-KEY-FROM-IGDB-OR-WHATEVER',
}

// Export config
export default config;
