/**
 * @name Config
 * @desc This file contains the configurations for the application.
 * @since May 29 2017
 */

// Create the config object
let config = {
    // Debug
    debug: true,

    // API settings
    wrapditUrl: 'http://127.0.0.1:5000/',
    igdbUrl: 'https://igdbcom-internet-game-database-v1.p.mashape.com/',
    igdbKey: '05COtQ20PZmshnL4AzcFMydjvkjDp1tH6eGjsnVwmoDH27NUcg',
    igdbImageUrl: 'https://images.igdb.com/igdb/image/upload/',
}

// Export config
export default config;
