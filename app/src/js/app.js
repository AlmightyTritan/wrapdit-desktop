/**
 * @name App
 * @desc This is the main render process file. It will handle prety much every
 *      thing we throw at it.
 * @since Dec 10 2016
 */

// Imports
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueElectron from 'vue-electron';
import path from 'path';
import { settings } from './settings.js';

import Game from './models/Game.js';
import Record from './models/Record.js';
import Session from './models/Session.js';

import AppRoot from 'app/src/vue/components/AppRoot.vue';
import QueueView from 'app/src/vue/views/QueueView.vue';
import SearchView from 'app/src/vue/views/SearchView.vue';
import UserView from 'app/src/vue/views/UserView.vue';
import GameView from 'app/src/vue/views/GameView.vue';
import SettingsView from 'app/src/vue/views/SettingsView.vue';
import LoginView from 'app/src/vue/views/LoginView.vue';
import LoadingView from 'app/src/vue/views/LoadingView.vue';
import RegisterView from 'app/src/vue/views/RegisterView.vue';
import PassResetView from 'app/src/vue/views/PassResetView.vue';

// Vue Plugins
Vue.use(VueRouter);
Vue.use(VueElectron);

// Variables
let routes = [
    { path: '/queue', component: QueueView },
    { path: '/search', component: SearchView },
    { path: '/settings', component: SettingsView },
    { path: '/user', component: UserView },
    { path: '/game', component: GameView },
    { path: '/login', component: LoginView },
    { path: '/loading', component: LoadingView },
    { path: '/register', component: RegisterView },
    { path: '/password-reset', component: PassResetView }
];

// Init vue router
const router = new VueRouter({ routes });

// Init vue app
const app = new Vue({
    el: '#vue-root',
    router: router,
    components: {
        AppRoot
    }
});

app.$router.push({
    path: '/loading',
    query: {
        redirect: '/queue',
    },
});

// Exports
export default app;
