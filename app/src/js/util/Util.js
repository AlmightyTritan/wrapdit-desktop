/**
 * @name Util
 * @desc This class contains static utility methods.
 * @since Jan 30 2017
 */

class Util {
    /**
     * @desc This method will pad out a multi-dimentional array so all the
     *      nested arrays have then same length.
     * @since Jan 30 2017
     * @param Array array
     * @param Any content
     */
    static multiDimenArrayPad(array, content = 0) {
        // Get the length
        let maxLength = this.multiDimenArrayLength(array);

        // For each array pad them out
        array.forEach((a) => {
            while (a.length < maxLength) {
                a.push(content);
            }
        })
    }

    /**
     * @desc This method will get the largest length from multi-dimentional
     *      array.
     * @since Jan 30 2017
     * @param Array array
     * @returns Int
     */
    static multiDimenArrayLength(array) {
        return array.reduce((a, b) => {
            return Math.max(a, b.length);
        }, 0);
    }
}

// Exports
export default Util;
