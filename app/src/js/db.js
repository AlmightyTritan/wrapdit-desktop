/**
 * @name DB
 * @desc This is the database file. It will export the database and
 *      handle the set up of the database and schema.
 * @since Feb 24 2017
 */

// Imports
import Dexie from 'dexie';
import DexieObservable from 'dexie-observable';
import Relationships from 'dexie-relationships';
import config from './config/config.js';

// Enable debug?
Dexie.debug = true;

// Create a new DB connection
let db = new Dexie('OfflineDatabase', { addons: [DexieObservable, Relationships]});

// Set up database schema
db.version(1).stores({
    games: '$$uuid, &gameId, &igdbId, &title, ttbAverageNormal, ttbMedianNormal,'
        + 'ttbShortestNormal, ttbLongestNormal, ttbEntriesNormal, gsCompleted,'
        + 'gsAbandoned, gsActive, gsBacklog, coverImage, screenshots, about,'
        + 'ttbAverageIGDB, offlineOnly, pendingDelete, version',
    records: '$$uuid, &recordId, gameId -> games.gameId, completionStatus,'
        + 'offlineOnly, pendingDelete, version',
    sessions: '$$uuid, &sessionId, recordId -> records.recordId, duration, date,'
        + 'offlineOnly, pendingDelete, version',
});

// On Database change
db.on('changes', (changes) => {
    for (let change of changes) {
        switch (change.type) {
            // CREATED
            case 1:
                break;

            // UPDATED
            case 2:
                updateForeignKey(change);
                break;

            // DELETED
            case 3:
                break;
        }
    }
});

// Functions
/**
 * @desc This function will update any changes made to foreign keys
 * @since Apr 28 2017
 * @param Object databaseChange
 */
function updateForeignKey(databaseChange) {
    // Get relationships
    let relationships = getForeignKeys(databaseChange.table);

    // Loop through the modifications
    for (let key in databaseChange.mods) {
        // Loop through this tables foreign relations
        for (let relationship of relationships) {
            // If the modification matches a relationship
            if (key == relationship.index) {
                // Query the remote table for an update
                let updateQuery = db.table(relationship.foreignTable)
                    .where(relationship.foreignIndex)
                    .equals(databaseChange.oldObj[key])
                    .toArray();

                // On query complete
                updateQuery.then((results) => {
                    // Loop through the models
                    for (let model of results) {
                        // Modify the model
                        model[key] = databaseChange.mods[key];

                        // Save the changes
                        db.table(databaseChange.table).put(model);
                    }
                });
            }
        }
    }
}

/**
 * @desc This function will return an array of foreign key relationships that
 *      can be updated.
 * @since Apr 29 2017
 * @param String tableName
 * @returns Array relationships
 */
function getForeignKeys(tableName) {
    // Store relations
    let relationships = [];

    // Loop though the tables
    for (let table of db.tables) {
        // If the table is protected skip
        if (table.name.charAt(0) == '_') {
            continue;
        }

        // If the table is the one we are already in skip
        if (table.name == tableName) {
            continue;
        }

        // If table has no foreign keys skip
        if (table.schema.foreignKeys.length <= 0) {
            continue;
        }

        // Loop through the tables foreign keys
        for (let foreignKey of table.schema.foreignKeys) {
            // If the target table matches the table name add the relation
            if (foreignKey.targetTable == tableName) {
                // Create new relationship object
                let relationship = {
                    index: foreignKey.targetIndex,
                    foreignIndex: foreignKey.index,
                    foreignTable: table.name,
                };

                relationships.push(relationship);
            }
        }
    }

    // Return the array of relationships
    return relationships;
}

// Exports
export default db;
