// Imports
const path = require('path');
const url = require('url');
const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

// Variables
let mainWindow;

// Functions
/**
 * @desc This function will create the main window of the app.
 * @since Dec 11 2016
 */
function createWindow () {
    // Init main window
    mainWindow = new BrowserWindow({width: 1280, height: 800});

    // Disable the menu
    mainWindow.setMenu(null);

    // Load HTML file for the app
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Main window Event Bindings
    mainWindow.on('closed', mainWindowClosed);
}

/**
 * @desc This function is called when the main window is closed.
 * @since Dec 11 2016
 */
function mainWindowClosed() {
    // Dereference the window
    mainWindow = null;
}

/**
 * @desc This function is called when the app is ready
 * @since Dec 11 2016
 */
function appReady() {
    // Create the main window
    createWindow()

    // If Developement env
    if (process.env.NODE_ENV !== 'production') {
        // Enable Vue dev tools
        require('vue-devtools').install()

        // Display dev tools
        mainWindow.toggleDevTools();
    }
}

/**
 * @desc This function is called when all the windows of the app have
 *      been closed.
 * @since Dec 11 2016
 */
function appAllWindowsClosed() {
    // Keep active in OS X unless explicitly closed
    if (process.platform !== 'darwin') {
        app.quit()
    }
}

/**
 * @desc This function is called when the app is made active.
 * @since Dec 11 2016
 */
function appActive() {
    // Recreate window when made active from dock on OS X
    if (mainWindow === null) {
        createWindow()
    }
}

// Event Bindings
app.on('ready', appReady);
app.on('window-all-closed', appAllWindowsClosed);
app.on('activate', appActive);
